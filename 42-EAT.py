#!/usr/bin/python3

import websocket
import _thread
import threading
import sys
import time
import ssl
import sqlite3
import urllib.request
import socket
from datetime import date
from datetime import datetime

from enum import Enum


import logging

import telegram

from telegram import Update, ForceReply
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext


# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)

#def catch_error(f):
    #@wraps(f)
    #def wrap(bot, update):
    #    logger.info("User {user} sent {message}".format(user=update.message.from_user.username, message=update.message.text))
    #    try:
    #        return f(bot, update)
    #    except Exception as e:
    #        # Add info to error tracking
    #        logger.error(str(e))
    #        #bot.send_message(chat_id=update.message.chat_id,
    #        #                 text="An error occured ...")
    #        sys.exit()
#    return


def websocket_send(state, login, is_member, message):
    global wss
    wss.send("{\"%s\":%d,\"%s\":\"%s\",\"%s\":%d,\"%s\":\"%s\"}" % ("state", state, "login", login, "is_member", is_member, "message", message))


# Define a few command handlers. These usually take the two arguments update and
# context.
#@catch_error
def add(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /add is issued."""
    global mutex_state, state, login, chat_id, is_member, mutex_db
    mutex_state.acquire()
    if state == 0:
        try:
            mutex_db.acquire()
            login = context.args[0]
            cursor.execute(""" SELECT * FROM users WHERE login=?; """, (login,))
            if cursor.fetchone() != None:
                update.message.reply_text("This login : " + login + " Already exist on the database.")
                mutex_state.release()
                mutex_db.release()
                return
            is_member = context.args[1]
            mutex_db.release()
            state = 1
            update.message.reply_text("Next card scanned will be added with the login: " + login)
        except (IndexError, ValueError):
            update.message.reply_text('Usage: /add <login>, <member(0 || 1)>')
            mutex_db.release()
    else:
         update.message.reply_text("Busy, scan a card for the previous name added: " + login)
    mutex_state.release()

#@catch_error
def set_adherent(update: Update, context: CallbackContext) -> None:
    global cursor, conn, mutex_db
    try:
        if int(context.args[1]) != 0 and int(context.args[1]) != 1:
            raise ValueError("not correct member value")
        mutex_db.acquire()
        joining_date = ""
        if (context.args[1] == '1'):
            joining_date = str(date.today())
        cursor.execute(""" UPDATE users SET member=? WHERE login=?""", (context.args[1], context.args[0]))
        cursor.execute(""" UPDATE users SET joining_date=? WHERE login=?""", (joining_date, context.args[0]))
        conn.commit()
        mutex_db.release()
        update.message.reply_text("The member state of " + context.args[0] + " has been set to " + context.args[1])

    except (IndexError, ValueError):
        update.message.reply_text('Usage: /set_adherent <login> <member(0 || 1)>')

#@catch_error
def update_login(update: Update, context: CallbackContext) -> None:
    global cursor, conn, mutex_db
    mutex_db.acquire()
    try:
        cursor.execute(""" SELECT * FROM users WHERE id=?; """, (context.args[1],))
        if cursor.fetchone() == None:
            mutex_db.release()
            update.message.reply_text("The card uid " + context.args[1] +" does not exist on the database.")
            return
        cursor.execute(""" UPDATE users SET login=? WHERE id=?""", (context.args[0], context.args[1]))
        conn.commit()
        mutex_db.release()
        update.message.reply_text("The card " + context.args[1] + "'s login has been updated to " + context.args[0])

    except (IndexError, ValueError):
        update.message.reply_text('Usage: /update_login <new login> <card uid>')
    mutex_db.release()

#@catch_error
def update_card(update: Update, context: CallbackContext) -> None:
    global mutex_state, login, state, mutex_db
    mutex_state.acquire()
    if state == 0:
        try:
            login = context.args[0]
            mutex_db.acquire()
            cursor.execute(""" SELECT * FROM users WHERE login=?; """, (login,))
            if cursor.fetchone() == None:
                mutex_db.release()
                mutex_state.release()
                update.message.reply_text("The " + login +" does not exist on the database.")
                return
            mutex_db.release()
            state = 2
            update.message.reply_text("The " + login +"'s card will be updated at the next scanned card.")

        except (IndexError, ValueError):
            update.message.reply_text("Usage: /update_card <login>")
    mutex_state.release()

#@catch_error
def set_mod(update: Update, context: CallbackContext) -> None:
    global global_state, mutex_state
    mutex_state.acquire()
    try:
        mod = int(context.args[0])
        global_state = mod
        update.message.reply_text("Mod has been set to: " + str(mod))
    except (IndexError, ValueError):
        update.message.reply_text("Usage: /set_mod <mod(0 || 1)>")
    mutex_state.release()

#@catch_error
def coming(update: Update, context: CallbackContext) -> None:
    global global_state, mutex_state
    mutex_state.acquire()
    if global_state != 0:
        update.message.reply_text("Mod is not closed(0), this command is not available")
        mutex_state.release()
        return
    try:
        login = context.args[0]
        websocket_send(0, login, 0, "Is coming for you")
        update.message.reply_text("A message has been displayed on the bar-screen")
        global_state = 1
        update.message.reply_text("Mod has been set to: 1")
    except (IndexError, ValueError):
        update.message.reply_text("Usage: /coming <login>")
    mutex_state.release()

#@catch_error
def show_db(update: Update, context: CallbackContext) -> None:
    global cursor, mutex_db
    mutex_db.acquire()
    cursor.execute(""" SELECT * FROM users """)
    response = cursor.fetchall()
    mutex_db.release()
    update.message.reply_text("database : " + str(response))

#@catch_error
def get_chat_id(update: Update, context: CallbackContext) -> None:
    user = update.effective_user
    chat_id = update.message.chat_id
    update.message.reply_text(chat_id)

#@catch_error
def help_command(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /help is issued."""
    update.message.reply_text("add - Add new login and card to the data base (Usages : /add <login>). \
    \nhelp - Send this help. \
    \nshow_db - Show the actual database. \
    \nset_adherent - Modify the adherent state of the login (Usages : /set_adherent <login> <member(0 || 1)>). \
    \nupdate_card - Update the card uid of a specified login (Usages : /update_card <login>). \
    \nset_mod - Set the mod, if the bar is closed or opened (Usages: /set_mod <mod(0 || 1)>). \
    \ncoming - Send to the little screen on the bar that you are coming (Usages: /coming <login>). \
    \nupdate_login - Update login of a given uid card number (Usages: /update_login <new login> <card uid>).")

#@catch_error
def unknown(update: Update, context: CallbackContext) -> None:
    update.message.reply_text("Sorry, I didn't understand that command.")


#@catch_error
def main() -> None:
    """Start the bot."""
    global chat_id, bot

    # Create the Updater and pass it your bot's token.
    updater = Updater("1950138682:AAFQ-I9cmQypapaOXhSB_KX5vwPdmg2bOLU")
    bot = telegram.Bot(token='1950138682:AAFQ-I9cmQypapaOXhSB_KX5vwPdmg2bOLU')
    chat_id = -1001766029023
    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler("add", add))
    dispatcher.add_handler(CommandHandler("help", help_command))
    dispatcher.add_handler(CommandHandler("get_chat_id", get_chat_id))
    dispatcher.add_handler(CommandHandler("show_db", show_db))
    dispatcher.add_handler(CommandHandler("set_adherent", set_adherent))
    dispatcher.add_handler(CommandHandler("update_card", update_card))
    dispatcher.add_handler(CommandHandler("set_mod", set_mod))
    dispatcher.add_handler(CommandHandler("coming", coming))
    dispatcher.add_handler(CommandHandler("update_login", update_login))

    dispatcher.add_handler(MessageHandler(Filters.command, unknown))

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    # updater.idle()

#####################################################################################

def on_message(wss, message):
    global cursor, mutex_state, state, login, conn, chat_id, bot, mutex_db, is_member, global_state
    id = int(message)
    mutex_state.acquire()
    if (state == 1):
        mutex_db.acquire()
        joining_date = ""
        if (is_member == '1'):
            joining_date = str(date.today())
        data = {"id" : id, "login" : login, "member" : is_member, "joining_date" : joining_date}
        cursor.execute(""" INSERT INTO users(login, id, member, joining_date) VALUES(:login, :id, :member, :joining_date)""", data)
        conn.commit()
        mutex_db.release()
        websocket_send(1, login, int(is_member), "Welcome to 42-EAT")
        bot.send_message(chat_id=chat_id, text=login + " has been added to the database")
        state = 0;
    elif (state == 2):
        mutex_db.acquire()
        cursor.execute(""" UPDATE users SET id=? WHERE login=?""", (id, login))
        conn.commit()
        mutex_db.release()
        websocket_send(1, login, int(is_member), "Welcome to 42-EAT")
        bot.send_message(chat_id=chat_id, text=login + "'s card has been updated")
        state = 0;
    else:
        mutex_db.acquire()
        cursor.execute("""SELECT id, login FROM users WHERE id=?""", (id,))
        login = cursor.fetchone()
        cursor.execute("""SELECT id, member FROM users WHERE id=?""", (id,))
        is_member = cursor.fetchone()
        cursor.execute("""SELECT id, joining_date FROM users WHERE id=?""", (id,))
        joining_date = cursor.fetchone()
        mutex_db.release()
        this_date = datetime.strptime(date, "%Y-%m-%d")
        right_now = datetime.now()
        date_diff = this_date - right_now
        if global_state == 0:
            if (login != None):
                login = login[1]
                is_member = is_member[1]
                if is_member == '1':
                    if date_diff.days < 0:
                        is_member = '0'
                        mutex_db.acquire()
                        cursor.execute(""" UPDATE users SET member=? WHERE login=?""", (is_member, login))
                        conn.commit()
                        mutex_db.release()
                websocket_send(0, login, is_member, "A message has been sent to 42-EAT staff")
                bot.send_message(chat_id=chat_id, text=login + " with member state " + str(is_member) + " is waiting...")
            else:
                websocket_send(0, "Unknown", -1, "A message has been sent to 42-EAT staff")
                bot.send_message(chat_id=chat_id, text="Unknown is waiting...")
        elif global_state == 1:
            if (login != None):
                login = login[1]
                is_member = is_member[1]
                if is_member == '1':
                    if date_diff.days < 0:
                        is_member = '0'
                        mutex_db.acquire()
                        cursor.execute(""" UPDATE users SET member=? WHERE login=?""", (is_member, login))
                        conn.commit()
                        mutex_db.release()
                websocket_send(1, login, is_member, "Enjoy your meal ;)")
            else:
                websocket_send(1, "Unknown", -1, "Enjoy your meal ;)")
    mutex_state.release()

def on_error(wss, error):
    if isinstance(error, socket.timeout):
        print(error)
        return
    print(error)
    sys.exit()

def on_close(wss, close_status_code, close_msg):
    print("### closed ###")
    global wss_connected
    wss_connected = False
    # websocket_connect_to_esp()

def on_ping(wsapp, message):
    global last_ping
    last_ping = time.time()
    # print("Got a ping! A pong reply has already been automatically sent.")

def on_pong(wsapp, message):
    print("Got a pong! No need to respond")

def on_open(wss):
    print("### connected ###")
    global wss_connected, last_ping
    last_ping = time.time()
    wss_connected = True
    def run(*args):
        global wss_connected, last_ping
        while (wss_connected == True):
            # print (time.time() - last_ping)
            if time.time() - last_ping > 5:
                wss.close()
                wss_connected = False
                print("### closed ###")
            time.sleep(1)
    _thread.start_new_thread(run, ())

def websocket_connect_to_esp():
    global wss
    websocket.enableTrace(False)
    websocket.setdefaulttimeout(3)
    wss = websocket.WebSocketApp("wss://10.19.1.67/",
                                 on_open=on_open,
                                 on_message=on_message,
                                 on_error=on_error,
                                 on_close=on_close,
                                 on_ping=on_ping,
                                 on_pong=on_pong,)

    wss.run_forever(sslopt={"check_hostname": False, "cert_reqs": ssl.CERT_NONE})

def wait_for_internet_connection(url='http://google.com', timeout=8):
    while True:
        try:
            urllib.request.urlopen(url, timeout=timeout)
            return
        except Exception as e:
            pass

if __name__ == "__main__":
    wait_for_internet_connection()
    wss_connected = False
    last_pong = 0
    mutex_state = threading.Lock()
    mutex_db = threading.Lock()
    state = 0 # is adding user or not
    global_state = 0 # is bar is open
    login = ""
    chat_id = 0
    bot = 0
    is_member = False
    wss = 0
    conn = sqlite3.connect('/home/bde/42-EAT/1.Database/database.db', check_same_thread=False)
    cursor = conn.cursor()
    mutex_db.acquire()
    cursor.execute("""
CREATE TABLE IF NOT EXISTS "users" (
	"login"	TEXT NOT NULL UNIQUE,
	"id"	INTEGER,
	"member"	BOOLEAN,
	"joining_date"	TEXT,
	PRIMARY KEY("login")
);     """)
    conn.commit()
    mutex_db.release()

    _thread.start_new_thread(main, ())
    while True:
        websocket_connect_to_esp()
    conn.close()
